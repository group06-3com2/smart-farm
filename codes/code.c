//Libraries ເອີ້ນໃຊ້ DHT and LCD
#include <DHT.h>;
#include <LiquidCrystal.h>;

//Constants : ຄ່າຄົງທີ່
#define DHTPIN 2     // ກຳນົດໃຫ້ຂາ digital (2) ເຊື່ອມຕໍ່ກັບເຊັນເຊີ DHT22 
#define DHTTYPE DHT22   //  ກຳນົດປະເພດຂອງເຊັນເຊີ DHT ເປັນ DHT22
DHT dht(DHTPIN, DHTTYPE); // ສ້າງ instant class 'DHT' ຊື່ 'dht' ເຊິ່ງຈະໃຊ້ອ່ານຂໍ້ມູນຈາກເຊັນເຊີ
const int rs = 12, en = 11, d4 = 10, d5 = 9, d6 = 8, d7 = 7; // ກຳນົດຂາທີ່ໃຊ້ເຊື່ອມຕໍ່ກັບຈໍ LCD ເຂົ້າກັບ Arduino
LiquidCrystal lcd(12, 11, 10, 9, 8, 7); // ສ້າງ instant class 'LiquidCrystal' ຊື່ 'lcd' ດ້ວຍການກຳນົດຂາໄຟເຂົ້າ

//Variables : ຕົວແປ
float hum;  // ເກັບຄ່າຄວາມຊື້ນ
float temp; // ເກັບຄ່າອຸນຫະພູມ

//ຟັງຊັນການຕັ້ງຄ່າ
void setup()
{
  lcd.begin(16, 2);
  Serial.begin(9600);
	dht.begin();
}

//ຟັງຊັນການທຳງານ
void loop()
{
    // ອ່ານຂໍ້ມູນ ແລະ ເກັບໄວ້ໃນຕົວແປ hum ແລະ temp
    hum = dht.readHumidity(); // ອ່ານຄ່າຄວາມຊື້ນຈາກເຊັນເຊີ DHT22 ແລະ ເກັບໃນຕົວແປ hum
    temp= dht.readTemperature(); // ອ່ານຄ່າອຸນຫະພູມຈາກເຊັນເຊີ DHT22 ແລະ ເກັບໄວ້ໃນຕົວແປ temp
    lcd.setCursor(0, 0); //  ຕັ້ງຕຳແໜ່ງ cursor ໄປທີ່ແຖວທຳອິດເທິງໜ້າຈໍ LCD
    lcd.print("Temp:"); //  ສະແດງຂໍ້ຄວາມ "Temp:" ທີ່ຕຳແໜ່ງ cursor ປະຈຸບັນ
    lcd.print(temp); // ສະແດງຄ່າອຸນຫະພູມ
    lcd.print(" C"); // ສະແດງຂໍ້ຄວາມ "C" ເພື່ອບອກວ່າເປັນຫົວໜ່ວຍອົງສາ
    lcd.setCursor(0, 1); // ຕັ້ງຕຳແໜ່ງ cursor ໄປທີ່ແຖວທີສອງເທິງໜ້າຈໍ LCD
    lcd.print("Humidity:"); //  ສະແດງຂໍ້ຄວາມ "Humidity:" ທີ່ຕຳແໜ່ງ cursor ປະຈຸບັນ
    lcd.print(hum); // ສະແດງຄ່າຄວາມຊື້ນ
    lcd.print("%"); // ສະແດງເຄື່ອງໝາຍເປີເຊັນ "%" ເພື່ອບອກວ່າເປັນຫົວໜ່ວຍຄວາມຊື້ນ
    delay(2000); // ລໍຖ້າ 2 ວິນາທີ
}
